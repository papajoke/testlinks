<?php

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
    'MSG'           => 'messages',
    'MSG_COUNT'     => 'total des messages',
    'SELECT_MONTH'  => 'Sélection d\'un mois',
    'LAST_DAYS'     => 'Derniers jours',
));
