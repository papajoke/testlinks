<?php
/**
 *
 * Top 10. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2017, papajoke
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace manjaro\testlinks\mcp;

/**
 * Top 10 MCP module info.
 */
class main_info
{
	function module()
	{
		return array(
			'filename'	=> '\manjaro\testlinks\mcp\main_module',
			'title'		=> 'MCP_MANJA_TITLE',
			'modes'		=> array(
				'front'	=> array(
					'title'	=> 'MCP_TEST_LINKS',
					'auth'	=> 'ext_manjaro/testlinks',
					'cat'	=> array('MCP_MAIN'),//array('MCP_MANJA_TITLE')
				),
			),
		);
	}
}
