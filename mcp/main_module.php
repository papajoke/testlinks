<?php
/**
 *
 * Top 10. An extension for the phpBB Forum Software package.
 *
 * @copyright (c) 2017, papajoke
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

namespace manjaro\testlinks\mcp;

/**
 * links MCP module.
 */
class main_module
{
    var $u_action;
    var $excludes = array(
        'manjaro' , 'archlinux','ubuntu.fr', 'ubuntu-fr',
        'github.com', 'github.org', 'gitlab.com', 'kernel.org', 'mozilla.org', 'wikipedia.org', 'google.fr', 'youtube.com',
        'xn--pp-oia.com' , 'hostingpics.net', 'postimg.org', 'postimg.cc', '.png', '.jpg', '.gif',
    );

    function main($id, $mode)
    {
        global $phpbb_container, $db;
        $user = $phpbb_container->get('user');
        $template = $phpbb_container->get('template');
        $request = $phpbb_container->get('request');

        $user->add_lang_ext('manjaro/testlinks', 'mcp_testlinks_front');
        $this->tpl_name = 'mcp_testlinks';
        $this->page_title = $user->lang('MCP_MANJA_TITLE');
        add_form_key('manja/testlinks');
        
        # WHERE post_edit_count > 0 # alors message a été mofifié !

        $sql="SELECT post_text, post_time, post_id, post_edit_count, P.poster_id, U.username_clean,
                    YEAR(from_unixtime(post_time)) as an, MONTH(from_unixtime(post_time)) as mois
                FROM phpbb_posts as P
                LEFT JOIN phpbb_users as U ON U.user_id=P.poster_id
                WHERE 
                    U.user_posts<6 AND
                    /* post_edit_count > 0 AND */
                    post_text LIKE '%<URL url=\"http%' 
                ORDER BY P.post_time ASC LIMIT 1000;";

        $result = $db->sql_query($sql);
        while ($row = $db->sql_fetchrow($result)) 
        {
            preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $row['post_text'], $match);
            $match = array_unique($match[0]);
            //print_r($match);
            foreach ($match as $key => $item) {
                // supprimer les url tronquées
                foreach ($match as $k => $url) {
                    if (strpos($item, $url) === 0 && $item != $url) {
                        unset($match[$k]);
                    }
                }
            }
            foreach ($match as $url) {
                if ($this->url_to_manage($url)) {
                    if ($row['mois']<10 &&  substr(strval($row['mois']), 0, 1) != '0') {
                        $row['mois']="0".$row['mois'];
                    }
                    $template->assign_block_vars('links', array(
                        'NAME' => $row['username_clean'],
                        'POSTERID' => $row['poster_id'],
                        'POSTID' => $row['post_id'],
                        'URL' => $url,
                        'DATE' => $row['an']."-".$row['mois'],
                        'AN' => $row['an'],
                    ));
                }
            }
        }
        $db->sql_freeresult($result);

        //$template->assign_var('U_POST_ACTION', $this->u_action);
    }

    private function url_to_manage(string $url) {
        foreach ($this->excludes as $exclude) {
            if (strpos($url, $exclude) !== false) {
                return false;
            }
        }
        return true;
    }    
}
